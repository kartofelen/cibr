#!/usr/local/bin/python2.7
#python search.py -i dataset/train/ukbench00000.jpg

import argparse as ap
import cv2
import imutils 
import numpy as np
import os
from sklearn.externals import joblib
from scipy.cluster.vq import *

from sklearn import preprocessing
import numpy as np

from pylab import *
from PIL import Image
from rootsift import RootSIFT

def search(path):
	# Get the path of the training set
	# parser = ap.ArgumentParser()
	# parser.add_argument("-i", "--image", help="Path to query image", required="True")
	# args = vars(parser.parse_args())

	# Get query image path
	image_path = path

	# Load the classifier, class names, scaler, number of clusters and vocabulary 
	im_features, image_paths, idf, numWords, voc = joblib.load("bof.pkl")
		
	# Create feature extraction and keypoint detector objects
	fea_det = cv2.xfeatures2d.SIFT_create()
	des_ext = cv2.xfeatures2d.SIFT_create()

	# List where all the descriptors are stored
	des_list = []

	im = cv2.imread(image_path)
	kpts = fea_det.detect(im)
	kpts, des = des_ext.compute(im, kpts)

	# rootsift
	#rs = RootSIFT()
	#des = rs.compute(kpts, des)

	des_list.append((image_path, des))   
		
	# Stack all the descriptors vertically in a numpy array
	descriptors = des_list[0][1]

	# 
	test_features = np.zeros((1, numWords), "float32")
	words, distance = vq(descriptors,voc)
	for w in words:
		test_features[0][w] += 1

	# Perform Tf-Idf vectorization and L2 normalization
	test_features = test_features*idf
	test_features = preprocessing.normalize(test_features, norm='l2')

	score = np.dot(test_features, im_features.T)
	rank_ID = np.argsort(-score)

	# Visualize the results
	figure()
	gray()
	subplot(5,4,1)
	imshow(im[:,:,::-1])
	axis('off')
	results = []
	# return rank_ID[0][:5]
	for i, ID in enumerate(rank_ID[0][0:16]):
		results.append("\\static"+ image_paths[ID][11:])
		# img = Image.open(image_paths[ID])
		# gray()
		# subplot(5,4,i+5)
		# imshow(img)
		# axis('off')
	# show()  
	return results

from flask import request, render_template, Flask
# import cv2

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
	return render_template('index.html', name="Home")

@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
	if request.method == 'POST':
		f = request.files['query']
		f.save('./static/test.jpg')
		result = search('./static/test.jpg')
		print(result)
		# img = cv2.imread('./test.jpg')
        # cv2.imshow("test",img)
        # cv2.waitKey(0)
	return render_template('index.html', images=result)

if __name__ == '__main__':
    app.run(debug=True,port=80,host='0.0.0.0')